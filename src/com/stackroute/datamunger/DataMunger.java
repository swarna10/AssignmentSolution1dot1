package com.stackroute.datamunger;

import java.util.Arrays;
import java.lang.String;
import java.util.Scanner;


public class DataMunger {

    public static void main(String[] args) {
        // read the query from the user into queryString variable
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the Query:");
        String queryString = scan.nextLine();

        // call the parseQuery method and pass the queryString variable as a parameter
        DataMunger dm = new DataMunger();
        dm.parseQuery(queryString);
        scan.close();

    }

    /*
     * we are creating multiple methods, each of them are responsible for extracting
     * a specific part of the query. However, the problem statement requires us to
     * print all elements of the parsed queries. Hence, to reduce the complexity, we
     * are using the parseQuery() method. From inside this method, we are calling
     * all the methods together, so that we can call this method only from main()
     * method to print the entire output in console
     */
    public void parseQuery(String queryString) {
        // call the methods
        getSplitStrings(queryString);
        getFile(queryString);
        getBaseQuery(queryString);
        getConditionsPartQuery(queryString);
        getConditions(queryString);
        getLogicalOperators(queryString);
        getFields(queryString);
        getOrderByFields(queryString);
        getGroupByFields(queryString);
        getAggregateFunctions(queryString);
    }

    /*
     * this method will split the query string based on space into an array of words
     * and display it on console
     */
    public String[] getSplitStrings(String queryString) {
        String [] queryWords = queryString.toLowerCase().split("\\s");
//        System.out.println("The words in Query are:");
//        for(String sr : queryWords) {
//            System.out.println(sr);
//        }

        return queryWords;
    }

    /*
     * extract the name of the file from the query. File name can be found after a
     * space after "from" clause. 
     * Note:
     * -----
     * CSV file can contain a field that contains from as a part of the column name. 
     * For eg: from_date,from_hrs etc.
     * 
     * Please consider this while extracting the file name in this method.
     */
    public String getFile(String queryString) {
        String temp1 = queryString.substring(queryString.indexOf(" from ")).trim();
        String fileName;
        if(queryString.contains(" where ")){
            fileName = temp1.substring(temp1.indexOf(' '), temp1.indexOf("where ")).trim();
        }
        else if(queryString.contains(" group by ")){
            fileName = temp1.substring(temp1.indexOf(' '), temp1.indexOf("group by ")).trim();
        }
        else if(queryString.contains(" order by ")){
            fileName = temp1.substring(temp1.indexOf(' '), temp1.indexOf("order by ")).trim();
        }
        else {
            fileName = temp1.substring(temp1.indexOf(' ')).trim();
        }
        //System.out.println("File name is: "+fileName);
        return fileName;
    }

    /*
     * This method is used to extract the baseQuery from the query string. BaseQuery
     * contains from the beginning of the query till the where clause
     * 
     * Note:
     * ------- 
     * 1. the query might not contain where clause but contain order by or
     *    group by clause 
     * 2. the query might not contain where, order by or group by clause 
     * 3. the query might not contain where, but can contain both group by
     *    and order by clause
     */
    public String getBaseQuery(String queryString) {

        String baseQuery =null;
        if(queryString.contains(" where "))
        {
            baseQuery = queryString.substring(0,queryString.indexOf("where "));
        }
        else if(queryString.contains(" order by ") && queryString.contains(" group by "))
        {
            baseQuery = queryString.substring(0,queryString.indexOf("group by "));
        }
        else if(queryString.contains(" order by "))
        {
            baseQuery = queryString.substring(0,queryString.indexOf("order by "));
        }
        else if( queryString.contains(" group by "))
        {
            baseQuery = queryString.substring(0,queryString.indexOf("group by "));
        }
        else 
        {
            baseQuery = queryString.substring(0);
        }
        //System.out.println("The base query is : "+baseQuery);
        return baseQuery;

    }

    /*
     * This method is used to extract the conditions part from the query string. The
     * conditions part contains starting from where keyword till the next keyword,
     * which is either group by or order by clause. In case of absence of both group
     * by and order by clause, it will contain till the end of the query string.
     * Note: 
     * ----- 
     * 1. The field name or value in the condition can contain keywords
     * as a substring. 
     * For eg: from_city,job_order_no,group_no etc. 
     * 2. The query might not contain where clause at all.
     */
    public String getConditionsPartQuery(String queryString) {

        String queryConditionsPart = null;
        if(queryString.contains(" where "))
        {
            String temp2 = queryString.substring(queryString.indexOf(" where ")).toLowerCase().trim();
            if( queryString.contains(" group by ") && queryString.contains(" order by "))
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' '),temp2.indexOf("group by "));
            }
            else if( queryString.contains(" group by "))
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' '),temp2.indexOf("group by "));
            }
            else if( queryString.contains(" order by "))
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' '),temp2.indexOf("order by "));
            }
            else
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' '));
            }
        }
        else
        {
            queryConditionsPart = null;
        }
        //System.out.println("The conditions are: "+queryConditionsPart);
        return queryConditionsPart;
    }

    /*
     * This method will extract condition(s) from the query string. The query can
     * contain one or multiple conditions. In case of multiple conditions, the
     * conditions will be separated by AND/OR keywords. 
     * for eg: 
     * Input: select city,winner,player_match from ipl.csv where season > 2014 and city
     * ='Bangalore'
     * 
     * This method will return a string array ["season > 2014","city ='Bangalore'"]
     * and print the array
     * 
     * Note: 
     * ----- 
     * 1. The field name or value in the condition can contain keywords
     * as a substring. 
     * For eg: from_city,job_order_no,group_no etc. 
     * 2. The query might not contain where clause at all.
     */
    public String[] getConditions(String queryString) {

        String queryConditionsPart = null;
        String [] queryConditions = null;
        if(queryString.contains(" where "))
        {
            String temp2 = queryString.substring(queryString.indexOf(" where ")).toLowerCase().trim();
            if( queryString.contains(" group by ") && queryString.contains(" order by "))
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' '),temp2.indexOf(" group by ")).trim();
            }
            else if( queryString.contains(" group by "))
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' '),temp2.indexOf(" group by ")).trim();
            }
            else if( queryString.contains(" order by "))
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' '),temp2.indexOf(" order by ")).trim();
            }
            else
            {
                queryConditionsPart = temp2.substring(temp2.indexOf(' ')).trim();
            }
        }
        else
        {
            queryConditionsPart = null;
        }

        if(queryConditionsPart != null)
        {
            if(queryConditionsPart.contains(" and ") && queryConditionsPart.contains(" or "))
            {
                queryConditions = queryConditionsPart.split(" and | or ");
            }
            else if(queryConditionsPart.contains(" and "))
            {
                queryConditions = queryConditionsPart.split(" and ");
            }
            else if(queryConditionsPart.contains(" or "))
            {
                queryConditions = queryConditionsPart.split(" or ");
            }
            else 
            {
                queryConditions = queryConditionsPart.split(" or ");
            }
        }
        else
        {
            queryConditions = null;
        }
//        System.out.println("The different Condiotion parts are: ");
//        for(String sr : queryConditions)
//        System.out.println(sr);

        return queryConditions;
    }

    /*
     * This method will extract logical operators(AND/OR) from the query string. The
     * extracted logical operators will be stored in a String array which will be
     * returned by the method and the same will be printed 
     * Note: 
     * ------- 
     * 1. AND/OR keyword will exist in the query only if where conditions exists and it
     * contains multiple conditions. 
     * 2. AND/OR can exist as a substring in the conditions as well. 
     * For eg: name='Alexander',color='Red' etc. 
     * Please consider these as well when extracting the logical operators.
     * 
     */
    public String[] getLogicalOperators(String queryString) {

        String [] operators = null;
        if(queryString.contains(" where "))
        {
            if(queryString.contains(" and ") && queryString.contains(" or "))
            {
                String [] temp1 = {"and","or"};
                operators = temp1;
            }
            else if(queryString.contains(" and "))
            {
                String [] temp1 = {"and"};
                operators = temp1;
            }
            else if(queryString.contains(" or "))
            {
                String [] temp1 = {"or"};
                operators = temp1;
            }
            else
            {
                operators = null;
            }
        }
        else
        {
            operators = null;
        }
//        System.out.println("Logical operators used in the query are: ");
//        for(String sr : operators)
//            System.out.println(sr);
        return operators;

    }

    /*
     * This method will extract the fields to be selected from the query string. The
     * query string can have multiple fields separated by comma. The extracted
     * fields will be stored in a String array which is to be printed in console as
     * well as to be returned by the method
     * 
     * Note: 
     * ------ 
     * 1. The field name or value in the condition can contain keywords
     * as a substring. 
     * For eg: from_city,job_order_no,group_no etc. 
     * 2. The field name can contain '*'
     * 
     */
    public String[] getFields(String queryString) {

        String [] fields = null;
        String temp1 = queryString.substring(queryString.indexOf(" "),queryString.indexOf(" from ")).trim();
        if(temp1.contains("*"))
        {
        	fields = temp1.split("\\s");
        }
        else
        fields = temp1.split("\\s*,\\s*");

//        System.out.println("The fields to be selected are :");
//        for(String sr : fields)
//        System.out.println(sr);
        return fields;

    }

    /*
     * This method extracts the order by fields from the query string. 
     * Note: 
     * ------
     * 1. The query string can contain more than one order by fields. 
     * 2. The query string might not contain order by clause at all. 
     * 3. The field names,condition values might contain "order" as a substring. 
     * For eg:order_number,job_order 
     * Consider this while extracting the order by fields
     */
    public String[] getOrderByFields(String queryString) {
        String [] orderByFields = null;
        if(queryString.contains(" order by "))
        {
            String temp1 = queryString.substring(queryString.indexOf(" order by ")).trim();
            String temp2 = temp1.substring(temp1.indexOf(" by ")).trim();
            String temp3 = temp2.substring(temp2.indexOf(" ")).trim();
            orderByFields = temp3.split("\\s*,\\s*");
        }
        else
        {
            orderByFields = null;
        }
//        System.out.println("The Order by fields in the Queries are :");
//        for(String sr : orderByFields)
//        System.out.println(sr);
        return orderByFields;
    }

    /*
     * This method extracts the group by fields from the query string. 
     * Note: 
     * ------
     * 1. The query string can contain more than one group by fields. 
     * 2. The query string might not contain group by clause at all. 
     * 3. The field names,condition values might contain "group" as a substring. 
     * For eg: newsgroup_name
     * 
     * Consider this while extracting the group by fields
     */
    public String[] getGroupByFields(String queryString) {

        String [] groupByFields = null;
        if(queryString.contains(" group by "))
        {
            String temp1 = queryString.substring(queryString.indexOf(" group by ")).trim();
            String temp2 = temp1.substring(temp1.indexOf(" by ")).trim();
            String temp3 = null;
            if(queryString.contains(" order by "))
            {
                temp3 = temp2.substring(temp2.indexOf(" "),temp2.indexOf(" order by ")).trim();
            }
            else
            {
                temp3 = temp2.substring(temp2.indexOf(" ")).trim();
            }
            groupByFields = temp3.split("\\s*,\\s*");
        }
        else
        {
            groupByFields = null;
        }
//        System.out.println("The Gorup by fields in the Queries are :");
//        for(String sr : groupByFields)
//        System.out.println(sr);
        return groupByFields;
    }

    /*
     * This method extracts the aggregate functions from the query string. 
     * Note:
     * ------ 
     * 1. aggregate functions will start with "sum"/"count"/"min"/"max"/"avg"
     * followed by "(" 
     * 2. The field names might contain"sum"/"count"/"min"/"max"/"avg" as a substring.
     * For eg: account_number,consumed_qty,nominee_name
     * 
     * Consider this while extracting the aggregate functions
     */
    public String[] getAggregateFunctions(String queryString) {

    	String [] aggregateFunction = null;
    	if(queryString.contains("sum(") || queryString.contains("max(") || queryString.contains("min(") || queryString.contains("avg(") || queryString.contains("count("))
    	{
    		aggregateFunction = queryString.split("select")[1].split("from")[0].trim().split(",");
    	}
    	else
    	{
    		aggregateFunction = null;
    	}
    	
    	return aggregateFunction;
    }

}